// console.log('hello');
// const COLOR;  //SyntaxError
// const COLOR = 'red';
// COLOR = 'green'; //TypeError

// let value = 10;
// console.log(value);

// function letScope(){
//   let value = 20;
//   console.log(value);
// };

// letScope();

//Default function parameter

// function say(message='Hi'){
//     console.log(message);
// }

// say('hello world!');
// say();
// say(undefined);

//Rest parameter

// function restDemo(a,b,...args){
//     console.log(...args);
// }

// restDemo(1,2,3,4,5);

//Spread operator

// const odd = [1,3,5];
// const combined = [...odd,6,7,8];

// console.log(combined);

//Object literal

// const fname = 'Ankur';
// const lname = 'Patel';

// const user = {
//     fname , lname
// }

// console.log(user.fname);
// console.log(user.lname);

//for....of

// const marksArr = [70,80,90];

// for(let marks of marksArr){
//     console.log(marks);
// }

// const name = 'Ankur';
// for(let myName in name){
//     console.log(myName);
// }

//octal and binary literals

// const octalValue = 0o51; //051
// console.log(octalValue);

// const binaryValue = 0b111; //parseInt('111',2)
// console.log(binaryValue);

//template literals

// const aboutMe = `i'm ankur patel \` its me \n new line`; //single quote, escape, multiline string,string formatting, html code, variable & expression substitution
// // const aboutMe = `My name is ${name}`;
// console.log(aboutMe);

//Array destructuring

// function getResult(){
//     return [60,70,80];
// }

// const [stu1, stu2, stu3] = getResult();

// console.log(stu1);
// console.log(stu2);
// console.log(stu3);

// function details(){
//     return ['Ankur', 'Patel', ['Cricket', 'Badminton']];
// }

// let [fname, lname, [firstHobby, secondHobby]] = details();

// console.log(firstHobby);

//Object destructuring

// const person = {
//     firstName : 'Ankur',
//     lastName : 'Patel',
// }

// let { firstName, lastName , age = 21} = person;

// console.log(firstName);
// console.log(lastName);
// console.log(age);

// const person = {
//     firstName : 'Ankur',
//     lastName : 'Patel',
//     hobby : {
//         firstHobby : 'Cricket',
//         secondHobby : 'Badminton'
//     }
// }

// let{
//     hobby : {
//         firstHobby,
//         secondHobby
//     }, hobby
// } = person;

// console.log(hobby);

// import greeting from "./app.js";

// console.log(greeting);

//classes

// class Person {
//   constructor(name) {
//     this.name = name;
//   }
//   getName() {
//     return this.name;
//   }
// }

// const user = new Person('Ankur Patel')

// console.log(user.name);

//getter & setter

// class Person {
//   constructor(name) {
//     this._name = name;
//   }
//   get name() {
//     return this._name;
//   }

//   set name(newName) {
//     if (newName === "") {
//       throw "Error";
//     } else {
//       this._name = newName;
//     }
//   }
//   static totalMarks(a, b) {
//     return a + b;
//   }
// }

// const user = new Person("Ankur Patel");

// console.log(user.name);

// user.name = 'vivek';

// console.log(user.name);

// console.log(Person.totalMarks(50, 50));

//class expression

// class User {
//   constructor(firstName, lastName, age, hobby) {
//     this.fName = firstName;
//     this.lName = lastName;
//     this.fullName = () => {
//       return `Hello, i'm ${this.fName} ${this.lName}.`;
//     }
//     this.myAge = age;
//     this.myHobby = hobby;
//   }
//   get age() {
//     return `i'm ${this.myAge} year old.`;
//   }
// };

// const ankur = new User("Ankur", "Patel", 21, "Cricket");

// console.log(ankur.fullName());
// console.log(ankur.age);
// console.log(`My hobby is to play ${ankur.myHobby}.`);

//inheritence

// class Animal{
//   constructor(legs){
//     this.legs = legs;
//   }
//   walk(){
//     console.log(`Walk on ${this.legs} legs`);
//   }
// }

// class Bird extends Animal{
//   constructor(legs){
//     super(legs);
//   }
//   fly(){
//     console.log('Bird can fly');
//   }
// }

// let bird = new Bird(2);

// bird.walk();
// bird.fly();

//Symbols

// const userId = Symbol.for('userId');
// const enrollmentNo = Symbol.for('enrollmentNo');

// function * myFunction(){
//   let marks = [10,20,30,40];
//   console.log('hello');
//   yield marks;
// }

// const func = myFunction(); //generator object
// console.log(func.next());
// console.log(func.next());

// const users = [
//   { userId: 1, age: 20 },
//   { userId: 2, age: 21 },
//   { userId: 3, age: 22 },
//   { userId: 4, age: 23 },
// ];

// for (let i = 0; i < users.length; i++) {
//   console.log(users[i].userId);
// }

// for (const values of users) {
//   console.log(values.userId, values.age);
// }

// users.forEach((element) => {
//   console.log(element);
// });

// const arr1 = [10,20,30,40];

// for (;true;) {
//   console.log(arr1);
// }

// import { greeting } from "";

// console.log(greeting);

// const age = [];
// age[5] = 29;

// for (let i = 0; i < age.length; i++) {
//   const element = age[i];
//   console.log(element);
// }

// for(let i of age){
//   console.log(i);
// }

// age.forEach(element => {
//   console.log(element);
// })

// const marks = [10,20,30,40];
// let sum = 0;
// for (let i = 0; i < marks.length; i++) {
//   const element = marks[i];
//   sum += marks[i];
//   console.log(sum);
// }

// console.log(sum);

//Iteration

// const ratings = [
//     {user: 'John',score: 3},
//     {user: 'Jane',score: 4},
//     {user: 'David',score: 5},
//     {user: 'Peter',score: 2},
// ];

// for (const {user, score} of ratings) {
//     console.log(user, score)
// }

// const demoSet = new Set(['ankur', 'vivek', 'prince']);

// for (const name of demoSet) {
//     console.log(name);
// }

//Array Extensions

//Array of

//Normal array

// const arr1 = new Array(5);
// for (const element of arr1) {
//     console.log(element);
// }

// const arr2 = Array.of(2);

// for (const element of arr2) {
//     console.log(element);
// }

//Array from

// const userId = 'akp121';

// console.log(Array.from(userId))

// function printArguments() {
//     for (const args of arguments) {
//         console.log(Array.from(args))
//     }
// }

// printArguments('a','b','c','d');

// const oddNumbers = [1, 3, 5, 7, 9];

// const eventNumbers = Array.from(oddNumbers, (x) => x > 5);
// for (const number of eventNumbers) {
//   console.log(number);
// }

// Array.find

// const users = [{uId:'abc123', age:20},{uId:'def456', age:30},{uId:'ghi789', age:20}];

// console.log(users.find(e => e.age == 20));

// const marks = [50,60,70,80,90];

// console.log(marks.findIndex(element => element > 70));

//Object.assign

// const user = {
//     uId:'akp121',
//     age:20
// }

// const demoObject = {
//     a:1, b:2, c:3
// }

// const newObject = Object.assign({}, user, demoObject);
// newObject.d = 4;
// newObject.e = 5;
// console.log(newObject);

//String startsWith, endsWith & includes

// const greeting = "Hello all, Ankur Patel Here! and i'm 21 year old.";

// console.log(greeting.startsWith('Hello'));
// console.log(greeting.startsWith('Ankur',11));

// console.log(greeting.endsWith('Here!'));
// console.log(greeting.endsWith('Patel', 22));

// console.log(greeting.includes('all'));
// console.log(greeting.includes("all", 6));

// const nameFromGreeting = greeting.slice(11,22);
// console.log(nameFromGreeting);

// const splitedGreeting = greeting.split(' ');
// console.log(splitedGreeting);

// const updatedGreeting = greeting.replace('all', 'everyone');
// console.log(updatedGreeting);

// console.log(greeting.match(/\d+/g));

//Array.map()

// const arr1 = [1,2,3];
// const arr2 = arr1;
// console.log(`arr1 : ${arr1}`);
// console.log(`arr2 : ${arr2}`);
// console.log(`Both are equals : ${arr1===arr2}`);

// const oddNumberArray = [1,3,5,7,9];
// const eventNumbersArray = oddNumberArray.map(val => val);
// console.log(`oddNumberArray : ${oddNumberArray}`)
// console.log(`eventNumbersArray : ${eventNumbersArray}`)
// console.log(`Both are equals : ${oddNumberArray===eventNumbersArray}`);

//Even though each array contains the exact same elements, they are not the same reference and thus the oddNumberArray === numbers2 resolves to false.

//Array.filter()

// const numbers = [1,2,3,4,5,6,7,8,9,10];
// const even = numbers.filter(element => element%2 == 0);

// console.log(even);

//map creates a new array by transforming every element in an array individually. filter creates a new array by removing elements that don't belong.

// The map method is used to convert each item of an array, while the filter method is used to select certain items of an array.

const a = "Hello02881/";
// console.log(a.split(/[^0-9]/))

console.log(a.split(/[^0-9]/).join(""));
